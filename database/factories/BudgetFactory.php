<?php

namespace Database\Factories;

use App\Models\Budget;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class BudgetFactory extends Factory
{
    protected $model = Budget::class;

    public function definition()
    {
        $date = Carbon::now()->subDays(rand(0, 365));
        $userId = User::inRandomOrder()->first()->id;

        return [
            'title' => $this->faker->name,
            'start_date' => $date->format('Y-m-d'),
            'end_date' => $date->addMonth(1)->format('Y-m-d'),
            'user_id' => $userId,
        ];
    }
}
