<?php

namespace Database\Factories;

use App\Models\Budget;
use App\Models\Expense;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExpenseFactory extends Factory
{
    protected $model = Expense::class;

    public function definition()
    {
        $date = Carbon::now()->subDays(rand(0, 365))->format('Y-m-d');
        $budgetId = Budget::inRandomOrder()->first()->id;

        return [
            'amount' => $this->faker->randomFloat(2, 1, 10000),
            'description' => $this->faker->sentence,
            'category' => $this->faker->name,
            'date' => $date,
            'budget_id' => $budgetId,
        ];
    }
}
