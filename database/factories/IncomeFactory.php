<?php

namespace Database\Factories;

use App\Models\Budget;
use App\Models\Income;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class IncomeFactory extends Factory
{
    protected $model = Income::class;

    public function definition()
    {
        $date = Carbon::now()->subDays(rand(0, 365))->format('Y-m-d');
        $budgetId = Budget::inRandomOrder()->first()->id;

        return [
            'amount' => $this->faker->randomFloat(2, 1, 10000),
            'description' => $this->faker->sentence,
            'date' => $date,
            'budget_id' => $budgetId,
        ];
    }
}
