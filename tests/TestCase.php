<?php

namespace Tests;

use App\Models\Budget;
use App\Models\Income;
use App\Models\Expense;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();

        $user = User::inRandomOrder()->first();

        $this->actingAs($user);
    }

    public function makeBudget()
    {
        $income = Budget::factory()->make();

        return [
            'title' => $income->title,
            'start_date' => $income->start_date,
            'end_date' => $income->end_date,
            'user_id' => $income->user_id
        ];
    }

    public function makeIncome()
    {
        $income = Income::factory()->make();

        return [
            'amount' => $income->amount,
            'description' => $income->description,
            'date' => $income->date,
            'budget_id' => $income->budget_id
        ];
    }

    public function makeExpense()
    {
        $income = Expense::factory()->make();

        return [
            'amount' => $income->amount,
            'description' => $income->description,
            'category' => $income->category,
            'date' => $income->date,
            'budget_id' => $income->budget_id
        ];
    }

}
