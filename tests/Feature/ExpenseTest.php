<?php

namespace Tests\Feature;

use Tests\TestCase;

class ExpenseTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_expense_index()
    {
        $this->getJson('api/expenses')
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => ['*' => [
                    'amount',
                    'description',
                    'category',
                    'date',
                    'budget_id'
                ]],
            ]);
    }

    public function test_expense_store()
    {
        $expense = $this->makeExpense();

        $response = $this->postJson(route('expenses.store'), $expense)
            ->assertValid()
            ->assertSuccessful()
            ->assertJsonFragment($expense);

        $this->assertDatabaseHas('expenses', $expense);

        return $response['data']['id'];
    }

    /**
     * @depends test_expense_store
     */

    public function test_expense_update($id)
    {
        $expense = $this->makeExpense();

        $this->patchJson(route('expenses.update', $id), $expense)
            ->assertValid()
            ->assertSuccessful()
            ->assertJsonFragment($expense);

        $expense['id'] = $id;

        $this->assertDatabaseHas('expenses', $expense);
    }

    /**
     * @depends test_expense_store
     */

    public function test_expense_destroy($id)
    {
        $this->deleteJson(route('expenses.destroy', $id))
            ->assertSuccessful();

        $this->assertDatabaseMissing('expenses', ['id' => $id]);
    }
}
