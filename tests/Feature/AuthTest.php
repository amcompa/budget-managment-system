<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class AuthTest extends TestCase
{
    public function test_auth()
    {
        $user = User::factory()->create();

        $this->actingAs($user)
            ->getJson('api/user')
            ->assertSuccessful();
    }
}
