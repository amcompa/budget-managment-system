<?php

namespace Tests\Feature;

use Tests\TestCase;

class IncomeTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_income_index()
    {
        $this->getJson('api/incomes')
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => ['*' => [
                    'amount',
                    'description',
                    'date',
                    'budget_id'
                ]],
            ]);
    }

    public function test_income_store()
    {
        $income = $this->makeIncome();

        $response = $this->postJson(route('incomes.store'), $income)
            ->assertValid()
            ->assertSuccessful()
            ->assertJsonFragment($income);

        $this->assertDatabaseHas('incomes', $income);

        return $response['data']['id'];
    }

    /**
     * @depends test_income_store
     */

    public function test_income_update($id)
    {
        $income = $this->makeIncome();

        $this->patchJson(route('incomes.update', $id), $income)
            ->assertValid()
            ->assertSuccessful()
            ->assertJsonFragment($income);

        $income['id'] = $id;

        $this->assertDatabaseHas('incomes', $income);
    }

    /**
     * @depends test_income_store
     */

    public function test_income_destroy($id)
    {
        $this->deleteJson(route('incomes.destroy', $id))
            ->assertSuccessful();

        $this->assertDatabaseMissing('incomes', ['id' => $id]);
    }
}
