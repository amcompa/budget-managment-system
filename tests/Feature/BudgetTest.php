<?php

namespace Tests\Feature;

use Tests\TestCase;

class BudgetTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_budget_index()
    {
        $this->getJson('api/budgets')
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => ['*' => [
                    'title',
                    'start_date',
                    'end_date',
                    'user_id'
                ]],
            ]);
    }

    public function test_budget_store()
    {
        $budget = $this->makeBudget();

        $response = $this->postJson(route('budgets.store'), $budget)
            ->assertValid()
            ->assertSuccessful()
            ->assertJsonFragment($budget);

        $this->assertDatabaseHas('budgets', $budget);

        return $response['data']['id'];
    }

    /**
     * @depends test_budget_store
     */

    public function test_budget_update($id)
    {
        $budget = $this->makeBudget();

        $this->patchJson(route('budgets.update', $id), $budget)
            ->assertValid()
            ->assertSuccessful()
            ->assertJsonFragment($budget);

        $budget['id'] = $id;

        $this->assertDatabaseHas('budgets', $budget);
    }

    /**
     * @depends test_budget_store
     */

    /*public function test_budget_destroy($id)
    {
        $this->deleteJson(route('budgets.destroy', $id))
            ->assertSuccessful();

        $this->assertDatabaseMissing('budgets', ['id' => $id]);
    }*/

    public function test_user_budgets()
    {
        $this->getJson('api/budgets/user')
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => ['*' => [
                    'username',
                    'budgets' => ['*' => [
                        'id',
                        'title',
                        'incomesTotal',
                        'expensesTotal',
                        'balance'
                    ]],
                ]]
            ]);
    }
}
