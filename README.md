# Budget Management System

## Установка и запуск
```
git pull https://gitlab.com/amcompa/budget-managment-system.git
```
```
php artisan serve
```

## Настройка
#### В .env добавить данные подключения к базе данных

```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=<DATABASE_NAME>
DB_USERNAME=<USER_NAME>
DB_PASSWORD=<PASSWORD>
```

#### Провести миграции
```
php artisan migrate
```

## Добавить пользователя
```
php artisan tinker
```
```
User::create(['name' => 'user', 'email' => 'user@mail.ru', 'password' => Hash::make('123')])
```

## Провести тесты
```
php artisan test
```
