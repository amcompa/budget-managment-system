<?php

namespace App\Http\Controllers;

use App\Http\Requests\IncomeRequest;
use App\Models\Income;
use Symfony\Component\HttpFoundation\Response;

class IncomeController extends Controller
{
    public function index()
    {
        $incomes = Income::query()
            ->with('budget')
            ->orderBy('budget_id')
            ->get();

        return response()->successResponse($incomes);
    }

    public function store(IncomeRequest $request)
    {
        try {
            $income = Income::create($request->validated());

            return response()->successResponse($income);
        } catch (\Exception $exception) {
            return response()->errorResponse($exception->getMessage(), $exception->getCode());
        }
    }

    public function update(IncomeRequest $request, Income $income)
    {
        try {
            $this->checkIfExists($income);

            $income->update($request->validated());

            return response()->successResponse($income);
        } catch (\Exception $exception) {
            return response()->errorResponse($exception->getMessage(), $exception->getCode());
        }
    }

    public function destroy(Income $income)
    {
        try {
            $this->checkIfExists($income);

            $income->delete();

            return response()->successResponse('success');
        } catch (\Exception $exception) {
            return response()->errorResponse($exception->getMessage(), $exception->getCode());
        }
    }

    private function checkIfExists($income): void
    {
        if (!$income) {
            throw new \Exception('Income not found', Response::HTTP_NOT_FOUND);
        }
    }
}
