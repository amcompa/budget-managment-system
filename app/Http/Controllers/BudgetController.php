<?php

namespace App\Http\Controllers;

use App\Http\Requests\BudgetRequest;
use App\Models\Budget;
use App\Services\BudgetService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BudgetController extends Controller
{
    private BudgetService $service;

    public function __construct(BudgetService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $budgets = Budget::query()
            ->with([
                'incomes',
                'expenses'
            ])
            ->orderBy('title')
            ->get();

        return response()->successResponse($budgets);
    }

    public function store(BudgetRequest $request)
    {
        try {
            $budget = Budget::create($request->validated());

            return response()->successResponse($budget);
        } catch (\Exception $exception) {
            return response()->errorResponse($exception->getMessage(), $exception->getCode());
        }
    }

    public function update(BudgetRequest $request, Budget $budget)
    {
        try {
            $this->checkIfExists($budget);

            $budget->update($request->validated());

            return response()->successResponse($budget);
        } catch (\Exception $exception) {
            return response()->errorResponse($exception->getMessage(), $exception->getCode());
        }
    }

    public function destroy(Budget $budget)
    {
        try {
            $this->checkIfExists($budget);

            $budget->delete();

            return response()->successResponse('success');
        } catch (\Exception $exception) {
            return response()->errorResponse($exception->getMessage(), $exception->getCode());
        }
    }

    private function checkIfExists($budget): void
    {
        if (!$budget) {
            throw new \Exception('Budget not found', Response::HTTP_NOT_FOUND);
        }
    }

    public function getUserBudgets(Request $request)
    {
        $userBudgets = $this->service->getUserBudgets($request);

        return response()->successResponse($userBudgets);
    }
}
