<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExpenseRequest;
use App\Models\Expense;
use Symfony\Component\HttpFoundation\Response;

class ExpenseController extends Controller
{
    public function index()
    {
        $expenses = Expense::query()
            ->with('budget')
            ->orderBy('budget_id')
            ->get();

        return response()->successResponse($expenses);
    }

    public function store(ExpenseRequest $request)
    {
        try {
            $expense = Expense::create($request->validated());

            return response()->successResponse($expense);
        } catch (\Exception $exception) {
            return response()->errorResponse($exception->getMessage(), $exception->getCode());
        }
    }

    public function update(ExpenseRequest $request, Expense $expense)
    {
        try {
            $this->checkIfExists($expense);

            $expense->update($request->validated());

            return response()->successResponse($expense);
        } catch (\Exception $exception) {
            return response()->errorResponse($exception->getMessage(), $exception->getCode());
        }
    }

    public function destroy(Expense $expense)
    {
        try {
            $this->checkIfExists($expense);

            $expense->delete();

            return response()->successResponse('success');
        } catch (\Exception $exception) {
            return response()->errorResponse($exception->getMessage(), $exception->getCode());
        }
    }

    private function checkIfExists($expense): void
    {
        if (!$expense) {
            throw new \Exception('Expense not found', Response::HTTP_NOT_FOUND);
        }
    }
}
