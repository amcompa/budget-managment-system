<?php


namespace App\Mixins;

class ResponseFactoryMixin
{
    public function successResponse()
    {
        return function ($data) {
            return [
                'code' => 200,
                'data' => $data,
                'message' => 'ok'
            ];
        };
    }

    public function errorResponse()
    {
        return function ($message, $code, $errors = null) {
            return [
                'code' => $code,
                'data' => $errors,
                'message' => $message
            ];
        };
    }
}
