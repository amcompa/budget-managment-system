<?php

namespace App\Services;

use App\Models\User;

final class BudgetService
{
    public function getUserBudgets($request)
    {
        return User::query()
            ->with([
                'budgets' => function ($query) use ($request) {
                    if ($request->budget_id) {
                        $query->where('id', $request->budget_id);
                    }
                },
                'budgets.incomes',
                'budgets.expenses'
            ])
            ->where('id', $request->user()->id)
            ->get()
            ->map(function ($user) {
                $budgets = [];

                foreach ($user->budgets as $budget) {
                    $incomesTotal = $budget->incomes->sum('amount');
                    $expensesTotal = $budget->expenses->sum('amount');

                    $budgets[] = [
                        'id' => $budget->id,
                        'title' => $budget->title,
                        'incomesTotal' => $incomesTotal,
                        'expensesTotal' => $expensesTotal,
                        'balance' => $incomesTotal - $expensesTotal
                    ];
                }

                return [
                    'username' => $user['name'],
                    'budgets' => $budgets
                ];
            });
    }
}
